# Generated by Django 3.0.5 on 2022-05-20 06:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('categorias', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='categoria',
            name='descripcion',
            field=models.CharField(max_length=500, null=True),
        ),
    ]
