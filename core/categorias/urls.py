from django.contrib.auth.decorators import login_required
from django.urls import path
from core.categorias.views import *

app_name = 'categorias'

urlpatterns = [
    path('', login_required(CategoriaListView.as_view()), name='index'),
    path('crear/', login_required(CategoriaCreateView.as_view()), name='crear'),
    path('editar/<int:pk>/', login_required(CategoriaUpdateView.as_view()), name='editar'),
    path('eliminar/delete_ajax/', login_required(CategoriaDeleteAjax.as_view()), name='eliminar'),
]