from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, TemplateView
from core.categorias.models import Categoria
from core.categorias.forms import CategoriaForm
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect, JsonResponse

# Create your views here.

class CategoriaListView(ListView):
    model = Categoria
    template_name = 'categorias/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        context['user_group'] = str(user.groups.first())
        context['title'] = 'Categorías'
        return context


class CategoriaCreateView(CreateView):
    model = Categoria
    template_name = 'categorias/form.html'
    form_class = CategoriaForm
    success_url = reverse_lazy('categorias:index')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        user = self.request.user
        context['user_group'] = str(user.groups.first())

        context['title'] = 'Nueva Categoria'
        context['back_url'] = self.success_url
        context['action'] = 'new'
        return context

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            form.save()
            # messages.success(request, 'La categoría se registró correctamente')
            return HttpResponseRedirect(self.success_url)

        messages.error(request, form.errors)
        return self.render_to_response(self.get_context_data(form=form))


class CategoriaUpdateView(UpdateView):
    model = Categoria
    template_name = 'categorias/form.html'
    form_class = CategoriaForm
    success_url = reverse_lazy('categorias:index')

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        context['user_group'] = str(user.groups.first())
        context['title'] = 'Modificar Categoria'
        context['back_url'] = self.success_url
        context['action'] = 'edit'
        return context

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            form.save()
            # messages.success(request, 'La categoría se modificó correctamente')
            return HttpResponseRedirect(self.success_url)

        messages.error(request, form.errors)
        return self.render_to_response(self.get_context_data(form=form))


class CategoriaDeleteAjax(TemplateView):

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            cat = Categoria.objects.get(pk=request.POST['id'])
            cat.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)