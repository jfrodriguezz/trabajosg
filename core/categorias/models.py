from django.db import models
from django.db.models import Model

# Create your models here.

class Categoria(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=200)

    def __str__(self):
        return f'{self.nombre}: {self.descripcion}'

    def save(self, *args, **kwargs):
        if not self.id:
            count_cats = Categoria.objects.all().count()
            self.id = count_cats + 1
        super(Categoria, self).save(*args, **kwargs)
