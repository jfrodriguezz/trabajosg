# Pasos
* En core, ejecuto el comando "python manage.py startapp categorias"
* En config/settings.py agregar la app. En este caso: "'core.categorias',"
* Ejecuto los comandos "python manage.py makemigrations" y "python manage.py migrate"
* En config, agrego "path('categorias/', include('core.categorias.urls')),"